#---------- Variables globales ------------------------------
:global cliente
:global reusoFTTH
:global reusoRF
:global reusoDedicado
#---------- Variables locales ------------------------------
:local maxLimit "0/0";
:local limitAt "0/0";
:local plan [/ip firewall address-list get $cliente list]
:local reuso $reusoDedicado
:local comentarioCola ""
:if ($plan~"FTTH") do={  
    :set reuso $reusoFTTH
    :set comentarioCola "FTTH"
} else={
    :if ($plan~"Dedicado") do={
        :nothing
        :set comentarioCola "Dedicado"
    } else={
        :set reuso $reusoRF
        :set comentarioCola "RF"
    }
}
:do {
    :local descargaPlan [/queue type get [find name=($plan . "_down")] pcq-rate]
    :local cargaPlan [/queue type get [find name=($plan . "_up")] pcq-rate]
    :set maxLimit ($cargaPlan . "/" . $descargaPlan )
    :set limitAt (($cargaPlan / $reuso) . "/" . ($descargaPlan / $reuso))
} on-error={ 
    :put ("No existe un queue type para el plan " . $plan)
};

#Agregamos la cola simple
:do {
    /queue simple add name=[/ip firewall address-list get $cliente comment] max-limit=$maxLimit limit-at=$limitAt parent=none\
        queue="default" target=[/ip firewall address-list get $cliente address] comment=$comentarioCola;
} on-error={
    :put ("Problemas agregando la cola de " . [/ip firewall address-list get $cliente comment])
};