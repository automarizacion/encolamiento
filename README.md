Programa para la configuración de un árbol de colas simples


El programa comprende 4 scripts más un sexto que los va llamando en orden. El script principal, donde se configuran los parámetros del árbol de colas simples, se llama calculadoraColasSimples. Los parámetros a configurar son:

- reusoRF : el reuso o divisor para los clientes RF
- reusoFTTH : el reuso o divisor para los clientes FTTH
- reusoDedicado : el reuso o divisor para los clientes dedicados
- cantClientesXCola : La cantidad de colas simples hijas por cada cola simple madre


El diagrama de flujo comprende:

    colasSimples
        |
	comprobarColas -> ordenarColas -> targetSQ


Primero, se comprueba que todos los clientes en /ip firewall address-list tengan una cola creada en /queue simple. Esta comprobación actualiza un arreglo global llamado clientesFaltantes. Este arreglo se recorre en un ciclo for para crear todas las colas faltantes. En función de hacer más fácil el ordenamiento de las colas, cada cola se crea con un comentario correspondiente al tipo de cliente: "FTTH", "RF" o "Dedicado". De esta manera, todos los clientes "FTTH" se agrupan en colas madres "FTTH"; haciéndose lo correspondiente para los clientes "RF" y "Dedicados".


