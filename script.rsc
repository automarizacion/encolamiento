:global cliente ""
:local clientesActuales [/ip firewall address-list find ((list~"Residencial" || list~"Pyme" || list~"Community" || list~"Dedicado") && disabled=no && !(address in 2.2.2.0/24))]
:local colasActuales [/queue simple find get]

:foreach c in=$clientesActuales do={
    :set cliente $c
    /system script run crearCola
    :put ("Creada la cola de " . [/ip firewall address-list get $c comment])    
}