#--------------------------------------------------------------------------------------------------------------
#  CALCULO TARGET Y MAX-LIMIT
:local colasMadres [/queue simple find (parent=none && name~"automatica") ]
:local colasHijas [:toarray  ""]
:local targetTotal [:toarray  ""]
:local limitDescarga 0
:local maxLimitDescarga 0
:local maxDescargaTotal 0
:local acumDescarga 0
:local multiplicador 1
:local limitAt ""
:local maxLimit ""
:local valorM false
:local valorK false

:foreach colaMadre in $colasMadres do={
    :set acumDescarga 0
	:set maxDescargaTotal 0
    :set targetTotal [:toarray  ""]
    :set colasHijas [/queue simple find parent=[/queue simple get $colaMadre name] ]
    :foreach i,colaHija in $colasHijas do={
		:set ($targetTotal->$i) ([/queue simple get $colaHija target])
        :set limitAt [/queue simple get $colaHija limit-at]
        #Se acumula la descarga minima
        :set multiplicador 1
        :set limitDescarga [:pick $limitAt ([:find $limitAt "/"]+1) [:len $limitAt]]
		:set valorM ([:find $limitDescarga "M"] > 0)
        :set valorK ([:find $limitDescarga "k"] > 0)
        :if ($valorM) do={ 
            :set multiplicador 1000000
            :set limitDescarga [:pick $limitDescarga 0 ([:len $limitDescarga]-1)]
        }
        :if ($valorK) do={ 
            :set multiplicador 1000 
            :set limitDescarga [:pick $limitDescarga 0 ([:len $limitDescarga]-1)]
        }
        :set limitDescarga ($limitDescarga * $multiplicador )
        #acumulamos descarga
        :set acumDescarga ($acumDescarga + $limitDescarga)
		#Se determina y compara la descarga maxima
		:set multiplicador 1
		:set maxLimit [/queue simple get $colaHija max-limit]
        :set maxLimitDescarga [:pick $maxLimit ([:find $maxLimit "/"]+1) [:len $maxLimit]]
        :set valorM ([:find $maxLimitDescarga "M"] > 0)
        :set valorK ([:find $maxLimitDescarga "k"] > 0)
        :if ($valorM) do={ 
            :set multiplicador 1000000
            :set maxLimitDescarga [:pick $maxLimitDescarga 0 ([:len $maxLimitDescarga]-1)]
        }
        :if ($valorK) do={ 
            :set multiplicador 1000 
            :set maxLimitDescarga [:pick $maxLimitDescarga 0 ([:len $maxLimitDescarga]-1)]
        }
        :set maxLimitDescarga ($maxLimitDescarga * $multiplicador )
		:if ([:tonum $maxLimitDescarga] > $maxDescargaTotal) do={ :set maxDescargaTotal [:tonum $maxLimitDescarga] }
    }
	:if ($acumDescarga > $maxDescargaTotal ) do={
		[/queue simple set target=$targetTotal max-limit=(($acumDescarga/1000000) ."M/" .  ($acumDescarga/1000000) . "M") numbers=$colaMadre]
	} else={
		[/queue simple set target=$targetTotal max-limit=(($maxDescargaTotal/1000000) ."M/" .  ($maxDescargaTotal/1000000) . "M") numbers=$colaMadre]
	}
}
#--------------------------------------------------------------------------------------------------------------