:global reusoRF 10
:global reusoDedicado 1
:global reusoFTTH 60
:global cantClientesXCola 100
:global comprobarColas true
#--------------------------------------------------------------------------------------------------------------
# tiempo inicio
:local t [/system clock get time]
:set t ([:pick $t 0 2].":".[:pick $t 3 5].":".[:pick $t 6 8])
:put ("Inicio del script: " . $t)
:log info ("Corriendo Script de colas simples. Inicio del script: " . $t)
#-------------------------------------------------------------------------------------------
#   COMPROBAR COLAS RF
#Comprobacion de que todas las colas existen
/system script run comprobarColas
:delay 1s;
:put ("Se comprobaron las colas. [OK]")
:log info ("Se comprobaron las colas. [OK]")
#-------------------------------------------------------------------------------------------
#   CREAR COLAS FALTANTES
:global clientesFaltantes
:global cliente
:foreach c in=$clientesFaltantes do={
    :set cliente $c
    /system script run crearCola
    :delay 100ms;
}
:put ("Se crearon las colas faltantes. [OK]")
:log info ("Se crearon las colas faltantes. [OK]")
#-------------------------------------------------------------------------------------------
#  ORDENAR COLAS
#Determinamos los clientes actuales del router
/system script run ordenarColas
:delay 100ms;
:put ("Se ordenaron las colas. [OK]")
:log info ("Se ordenaron las colas. [OK]")
#-------------------------------------------------------------------------------------------
#  CALCULO TARGET Y MAX-LIMIT
/system script run targetSQ
:put ("Se realizo el calculo de max-limit y asignacion de target. [OK]")
:log info ("Se realizo el calculo de max-limit y asignacion de target. [OK]")
#--------------------------------------------------------------------------------------------------------------
# tiempo final
:local t [/system clock get time]
:set t ([:pick $t 0 2].":".[:pick $t 3 5].":".[:pick $t 6 8])
:put ("Finalizacion del script: " . $t)
:log info ("Finalizacion del script: " . $t)
#--------------------------------------------------------------------------------------------------------------