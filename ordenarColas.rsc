#--------------------------------------------------------------------------------------------------------------
#  ORDENAR COLAS
:global cantClientesXCola
#--------------------------------------------------------------------------------------------------------------
#Primero los clientes FTTH
:local tiposDeColas {"RF";"FTTH";"Dedicado"}

:foreach ciclo,tipo in=$tiposDeColas do={  
    :local colasSinParent [/queue simple find comment=$tipo ]
    :local colasMadres [/queue simple find name~("Cola " . $tipo . " automatica")]
    :local cantColasSinParent [:len $colasSinParent]
    :local cantColasMadres [:len $colasMadres]
    :local hayColasMadres ($cantColasMadres > 0)
    :local hayColasSinParent ($cantColasSinParent > 0)
    :local colasXAsignar {""}
    :local N 0
    :local cantHijas 0
    :local cantColasNecesarias 0

    :if ($hayColasSinParent) do={
        :if (!$hayColasMadres) do={
            #No hay colas madres. Creamos las necesarias
            :set cantColasNecesarias (($cantColasSinParent / $cantClientesXCola) + 1)
            #Crear colas madres
            :for i from=0 to=($cantColasNecesarias-1) do={
                /queue simple add name=(("Cola " . $tipo . " automatica ").($i+1)) target=("1.1.".$ciclo.".".$i) max-limit="1000M/1000M" \
                    queue=pcq-download-default/pcq-upload-default
            }

            :set colasMadres [:toarray ""]
            :set colasMadres [/queue simple find name~("Cola " . $tipo . " automatica")]
            :set cantColasMadres [:len $colasMadres]
            :put ("Se crearon " . $cantColasNecesarias . " colas " . $tipo . " madres.")
        }

        :foreach j,colaMadre in $colasMadres do={
            :if ($cantColasSinParent > 0) do={
                :set cantHijas [:len [/queue simple find parent=[/queue simple get $colaMadre name]] ]
                :set N ($cantClientesXCola - $cantHijas)
                :if ($N > 0) do={
                    :if ($N > $cantColasSinParent) do={
                        #asignamos todas las colas
                        /queue simple set comment="" parent=[/queue simple get $colaMadre name] numbers=$colasSinParent
                        :set cantColasSinParent 0
                    } else={
                        #asignamos N colas
                        :set colasXAsignar value=[:toarray [:pick $colasSinParent 0 $N]]
                        /queue simple set comment="" parent=[/queue simple get $colaMadre name] numbers=$colasXAsignar
                        :set colasSinParent [:toarray ""]
                        :set colasSinParent [/queue simple find comment=$tipo ]
                        :set cantColasSinParent [:len $colasSinParent]
                    }
                } else={
                    #La cola esta al maximo. Comprobamos si era la ultima cola
                    :if ($j = ($cantColasMadres-1) ) do={
                        :put ("Ultima. Ciclo " . ($j+1))
                        #Se agregan las colas madres necesarias
                        :set cantColasNecesarias (($cantColasSinParent / $cantClientesXCola) + 1)
                        #Crear colas madres
                        :for i from=0 to=($cantColasNecesarias-1) do={
                            :put ("Agregando " . ($cantColasNecesarias) . " colas madres.")
                            /queue simple add name=("Cola ".$tipo ." automatica ".($i+$j+2)) target=("2.1.".$ciclo.".".$i) max-limit="1000M/1000M" \
                                queue=pcq-download-default/pcq-upload-default
                        }
                        :local colasNuevas [/queue simple find target~("2.1.".$ciclo.".".$i)]
                        :foreach colaNueva in $colasNuevas do={
                            :if ($cantColasSinParent < $cantClientesXCola) do={
                                #asignamos todas las colas
                                /queue simple set comment="" parent=[/queue simple get $colaNueva name] numbers=$colasSinParent
                                :set cantColasSinParent 0

                            } else={
                                #asignamos cantClientesXCola colas
                                :set colasXAsignar value=[:toarray [:pick $colasSinParent 0 $cantClientesXCola]]
                                /queue simple set comment="" parent=[/queue simple get $colaNueva name] numbers=$colasXAsignar
                                :set colasSinParent [/queue simple find comment=$tipo ]
                                :set cantColasSinParent [:len $colasSinParent]
                            }
                        }
                    }
                }
            }
        }
    }
}
#--------------------------------------------------------------------------------------------------------------
