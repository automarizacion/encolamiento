:global clientesFaltantes [:toarray ""];

:local clientesActuales [/ip firewall address-list find ((list~"Residencial" || list~"Pyme" || list~"Community" || list~"Dedicado") && disabled=no && !(address in 2.2.2.0/24))];
:local colasActuales [/queue simple find]
:local existe false
:local indiceColaBorrar 0

:foreach i,c in=$clientesActuales do={
    :set existe false
    :foreach indice,cola in $colasActuales do={
        :if ([/ip firewall address-list get $c comment] = [/queue simple get $cola name]) do={ 
            :set existe true
            :set indiceColaBorrar $indice
        }
    }
    :if ($existe) do={
        #Limpiamos el arreglo de las colas si el indice a borrar no es el cero
        :if ($indiceColaBorrar > 0) do={
            :local N [:len $colasActuales]
            :if ($indiceColaBorrar < ($N-1)) do={
                :for j from=$indiceColaBorrar to=($N-2) do={
                    :set ($colasActuales->$j) ($colasActuales->($j+1))
                }
            }
            :set colasActuales value=[:toarray [:pick $colasActuales 0 ($N-1)]]
        }
    } else={
        :set clientesFaltantes ( $clientesFaltantes, $c );
    }
}
:put ( "Faltan ". [:len $clientesFaltantes] . " clientes.")