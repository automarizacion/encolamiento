/system script
add dont-require-permissions=yes name=calculadoraColasSimples owner=haraujo \
    policy=ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon \
    source=":global reusoRF 10\r\
    \n:global reusoDedicado 1\r\
    \n:global reusoFTTH 60\r\
    \n:global cantClientesXCola 100\r\
    \n:global comprobarColas true\r\
    \n:global cliente\r\
    \n#-----------------------------------------------------------------------\
    ---------------------------------------\r\
    \n# tiempo inicio\r\
    \n:local t [/system clock get time]\r\
    \n:set t ([:pick \$t 0 2].\":\".[:pick \$t 3 5].\":\".[:pick \$t 6 8])\r\
    \n:put (\"Inicio del script: \" . \$t)\r\
    \n:log info (\"Corriendo Script de colas simples. Inicio del script: \" . \
    \$t)\r\
    \n#-----------------------------------------------------------------------\
    --------------------\r\
    \n#   COMPROBAR COLAS RF\r\
    \n#Comprobacion de que todas las colas existen\r\
    \n/system script run comprobarColas\r\
    \n:delay 1s;\r\
    \n:put (\"Se comprobaron las colas. [OK]\")\r\
    \n:log info (\"Se comprobaron las colas. [OK]\")\r\
    \n#-----------------------------------------------------------------------\
    --------------------\r\
    \n#   CREAR COLAS FALTANTES\r\
    \n:global clientesFaltantes\r\
    \n:foreach c in=\$clientesFaltantes do={\r\
    \n    :set cliente \$c\r\
    \n    /system script run crearCola\r\
    \n    :delay 100ms;\r\
    \n}\r\
    \n\r\
    \n:put (\"Se crearon las colas faltantes. [OK]\")\r\
    \n:log info (\"Se crearon las colas faltantes. [OK]\")\r\
    \n#-----------------------------------------------------------------------\
    --------------------\r\
    \n#  ORDENAR COLAS\r\
    \n#Determinamos los clientes actuales del router\r\
    \n/system script run ordenarColas\r\
    \n:delay 100ms;\r\
    \n:put (\"Se ordenaron las colas. [OK]\")\r\
    \n:log info (\"Se ordenaron las colas. [OK]\")\r\
    \n#-----------------------------------------------------------------------\
    --------------------\r\
    \n#  CALCULO TARGET Y MAX-LIMIT\r\
    \n/system script run targetSQ\r\
    \n:put (\"Se realizo el calculo de max-limit y asignacion de target. [OK]\
    \")\r\
    \n:log info (\"Se realizo el calculo de max-limit y asignacion de target. \
    [OK]\")\r\
    \n#-----------------------------------------------------------------------\
    ---------------------------------------\r\
    \n# tiempo final\r\
    \n:local t [/system clock get time]\r\
    \n:set t ([:pick \$t 0 2].\":\".[:pick \$t 3 5].\":\".[:pick \$t 6 8])\r\
    \n:put (\"Finalizacion del script: \" . \$t)\r\
    \n:log info (\"Finalizacion del script: \" . \$t)\r\
    \n#-----------------------------------------------------------------------\
    ---------------------------------------"
add dont-require-permissions=no name=comprobarColas owner=haraujo policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source=":\
    global clientesFaltantes [:toarray \"\"];\r\
    \n\r\
    \n:local clientesActuales [/ip firewall address-list find ((list~\"Residen\
    cial\" || list~\"Pyme\" || list~\"Community\" || list~\"Dedicado\") && dis\
    abled=no && !(address in 2.2.2.0/24))];\r\
    \n:local colasActuales [/queue simple find]\r\
    \n:local existe false\r\
    \n:local indiceColaBorrar 0\r\
    \n\r\
    \n:foreach i,c in=\$clientesActuales do={\r\
    \n    :set existe false\r\
    \n    :foreach indice,cola in \$colasActuales do={\r\
    \n        :if ([/ip firewall address-list get \$c comment] = [/queue simpl\
    e get \$cola name]) do={ \r\
    \n            :set existe true\r\
    \n            :set indiceColaBorrar \$indice\r\
    \n        }\r\
    \n    }\r\
    \n    :if (\$existe) do={\r\
    \n        #Limpiamos el arreglo de las colas si el indice a borrar no es e\
    l cero\r\
    \n        :if (\$indiceColaBorrar > 0) do={\r\
    \n            :local N [:len \$colasActuales]\r\
    \n            :if (\$indiceColaBorrar < (\$N-1)) do={\r\
    \n                :for j from=\$indiceColaBorrar to=(\$N-2) do={\r\
    \n                    :set (\$colasActuales->\$j) (\$colasActuales->(\$j+1\
    ))\r\
    \n                }\r\
    \n            }\r\
    \n            :set colasActuales value=[:toarray [:pick \$colasActuales 0 \
    (\$N-1)]]\r\
    \n        }\r\
    \n    } else={\r\
    \n        :set clientesFaltantes ( \$clientesFaltantes, \$c );\r\
    \n    }\r\
    \n}\r\
    \n:put ( \"Faltan \". [:len \$clientesFaltantes] . \" clientes.\")"
add dont-require-permissions=yes name=ordenarColas owner=haraujo policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source="#\
    --------------------------------------------------------------------------\
    ------------------------------------\r\
    \n#  ORDENAR COLAS\r\
    \n:global cantClientesXCola\r\
    \n#-----------------------------------------------------------------------\
    ---------------------------------------\r\
    \n#Primero los clientes FTTH\r\
    \n:local tiposDeColas {\"RF\";\"FTTH\";\"Dedicado\"}\r\
    \n\r\
    \n:foreach ciclo,tipo in=\$tiposDeColas do={  \r\
    \n    :local colasSinParent [/queue simple find comment=\$tipo ]\r\
    \n    :local colasMadres [/queue simple find name~(\"Cola \" . \$tipo . \"\
    \_automatica\")]\r\
    \n    :local cantColasSinParent [:len \$colasSinParent]\r\
    \n    :local cantColasMadres [:len \$colasMadres]\r\
    \n    :local hayColasMadres (\$cantColasMadres > 0)\r\
    \n    :local hayColasSinParent (\$cantColasSinParent > 0)\r\
    \n    :local colasXAsignar {\"\"}\r\
    \n    :local N 0\r\
    \n    :local cantHijas 0\r\
    \n    :local cantColasNecesarias 0\r\
    \n\r\
    \n    :if (\$hayColasSinParent) do={\r\
    \n        :if (!\$hayColasMadres) do={\r\
    \n            #No hay colas madres. Creamos las necesarias\r\
    \n            :set cantColasNecesarias ((\$cantColasSinParent / \$cantClie\
    ntesXCola) + 1)\r\
    \n            #Crear colas madres\r\
    \n            :for i from=0 to=(\$cantColasNecesarias-1) do={\r\
    \n                /queue simple add name=((\"Cola \" . \$tipo . \" automat\
    ica \").(\$i+1)) target=(\"1.1.\".\$ciclo.\".\".\$i) max-limit=\"1000M/100\
    0M\" \\\r\
    \n                    queue=pcq-download-default/pcq-upload-default\r\
    \n            }\r\
    \n\r\
    \n            :set colasMadres [:toarray \"\"]\r\
    \n            :set colasMadres [/queue simple find name~(\"Cola \" . \$tip\
    o . \" automatica\")]\r\
    \n            :set cantColasMadres [:len \$colasMadres]\r\
    \n            :put (\"Se crearon \" . \$cantColasNecesarias . \" colas \" \
    . \$tipo . \" madres.\")\r\
    \n        }\r\
    \n\r\
    \n        :foreach j,colaMadre in \$colasMadres do={\r\
    \n            :if (\$cantColasSinParent > 0) do={\r\
    \n                :set cantHijas [:len [/queue simple find parent=[/queue \
    simple get \$colaMadre name]] ]\r\
    \n                :set N (\$cantClientesXCola - \$cantHijas)\r\
    \n                :if (\$N > 0) do={\r\
    \n                    :if (\$N > \$cantColasSinParent) do={\r\
    \n                        #asignamos todas las colas\r\
    \n                        /queue simple set comment=\"\" parent=[/queue si\
    mple get \$colaMadre name] numbers=\$colasSinParent\r\
    \n                        :set cantColasSinParent 0\r\
    \n                    } else={\r\
    \n                        #asignamos N colas\r\
    \n                        :set colasXAsignar value=[:toarray [:pick \$cola\
    sSinParent 0 \$N]]\r\
    \n                        /queue simple set comment=\"\" parent=[/queue si\
    mple get \$colaMadre name] numbers=\$colasXAsignar\r\
    \n                        :set colasSinParent [:toarray \"\"]\r\
    \n                        :set colasSinParent [/queue simple find comment=\
    \$tipo ]\r\
    \n                        :set cantColasSinParent [:len \$colasSinParent]\
    \r\
    \n                    }\r\
    \n                } else={\r\
    \n                    #La cola esta al maximo. Comprobamos si era la ultim\
    a cola\r\
    \n                    :if (\$j = (\$cantColasMadres-1) ) do={\r\
    \n                        :put (\"Ultima. Ciclo \" . (\$j+1))\r\
    \n                        #Se agregan las colas madres necesarias\r\
    \n                        :set cantColasNecesarias ((\$cantColasSinParent \
    / \$cantClientesXCola) + 1)\r\
    \n                        #Crear colas madres\r\
    \n                        :for i from=0 to=(\$cantColasNecesarias-1) do={\
    \r\
    \n                            :put (\"Agregando \" . (\$cantColasNecesaria\
    s) . \" colas madres.\")\r\
    \n                            /queue simple add name=(\"Cola \".\$tipo .\"\
    \_automatica \".(\$i+\$j+2)) target=(\"2.1.\".\$ciclo.\".\".\$i) max-limit\
    =\"1000M/1000M\" \\\r\
    \n                                queue=pcq-download-default/pcq-upload-de\
    fault\r\
    \n                        }\r\
    \n                        :local colasNuevas [/queue simple find target~(\
    \"2.1.\".\$ciclo.\".\".\$i)]\r\
    \n                        :foreach colaNueva in \$colasNuevas do={\r\
    \n                            :if (\$cantColasSinParent < \$cantClientesXC\
    ola) do={\r\
    \n                                #asignamos todas las colas\r\
    \n                                /queue simple set comment=\"\" parent=[/\
    queue simple get \$colaNueva name] numbers=\$colasSinParent\r\
    \n                                :set cantColasSinParent 0\r\
    \n\r\
    \n                            } else={\r\
    \n                                #asignamos cantClientesXCola colas\r\
    \n                                :set colasXAsignar value=[:toarray [:pic\
    k \$colasSinParent 0 \$cantClientesXCola]]\r\
    \n                                /queue simple set comment=\"\" parent=[/\
    queue simple get \$colaNueva name] numbers=\$colasXAsignar\r\
    \n                                :set colasSinParent [/queue simple find \
    comment=\$tipo ]\r\
    \n                                :set cantColasSinParent [:len \$colasSin\
    Parent]\r\
    \n                            }\r\
    \n                        }\r\
    \n                    }\r\
    \n                }\r\
    \n            }\r\
    \n        }\r\
    \n    }\r\
    \n}\r\
    \n#-----------------------------------------------------------------------\
    ---------------------------------------\r\
    \n"
add dont-require-permissions=yes name=targetSQ owner=haraujo policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source="#\
    --------------------------------------------------------------------------\
    ------------------------------------\r\
    \n#  CALCULO TARGET Y MAX-LIMIT\r\
    \n:local colasMadres [/queue simple find (parent=none && name~\"automatica\
    \") ]\r\
    \n:local colasHijas [:toarray  \"\"]\r\
    \n:local targetTotal [:toarray  \"\"]\r\
    \n:local limitDescarga 0\r\
    \n:local maxLimitDescarga 0\r\
    \n:local maxDescargaTotal 0\r\
    \n:local acumDescarga 0\r\
    \n:local multiplicador 1\r\
    \n:local limitAt \"\"\r\
    \n:local maxLimit \"\"\r\
    \n:local valorM false\r\
    \n:local valorK false\r\
    \n\r\
    \n:foreach colaMadre in \$colasMadres do={\r\
    \n    :set acumDescarga 0\r\
    \n\t:set maxDescargaTotal 0\r\
    \n    :set targetTotal [:toarray  \"\"]\r\
    \n    :set colasHijas [/queue simple find parent=[/queue simple get \$cola\
    Madre name] ]\r\
    \n    :foreach i,colaHija in \$colasHijas do={\r\
    \n\t\t:set (\$targetTotal->\$i) ([/queue simple get \$colaHija target])\r\
    \n        :set limitAt [/queue simple get \$colaHija limit-at]\r\
    \n        #Se acumula la descarga minima\r\
    \n        :set multiplicador 1\r\
    \n        :set limitDescarga [:pick \$limitAt ([:find \$limitAt \"/\"]+1) \
    [:len \$limitAt]]\r\
    \n\t\t:set valorM ([:find \$limitDescarga \"M\"] > 0)\r\
    \n        :set valorK ([:find \$limitDescarga \"k\"] > 0)\r\
    \n        :if (\$valorM) do={ \r\
    \n            :set multiplicador 1000000\r\
    \n            :set limitDescarga [:pick \$limitDescarga 0 ([:len \$limitDe\
    scarga]-1)]\r\
    \n        }\r\
    \n        :if (\$valorK) do={ \r\
    \n            :set multiplicador 1000 \r\
    \n            :set limitDescarga [:pick \$limitDescarga 0 ([:len \$limitDe\
    scarga]-1)]\r\
    \n        }\r\
    \n        :set limitDescarga (\$limitDescarga * \$multiplicador )\r\
    \n        #acumulamos descarga\r\
    \n        :set acumDescarga (\$acumDescarga + \$limitDescarga)\r\
    \n\t\t#Se determina y compara la descarga maxima\r\
    \n\t\t:set multiplicador 1\r\
    \n\t\t:set maxLimit [/queue simple get \$colaHija max-limit]\r\
    \n        :set maxLimitDescarga [:pick \$maxLimit ([:find \$maxLimit \"/\"\
    ]+1) [:len \$maxLimit]]\r\
    \n        :set valorM ([:find \$maxLimitDescarga \"M\"] > 0)\r\
    \n        :set valorK ([:find \$maxLimitDescarga \"k\"] > 0)\r\
    \n        :if (\$valorM) do={ \r\
    \n            :set multiplicador 1000000\r\
    \n            :set maxLimitDescarga [:pick \$maxLimitDescarga 0 ([:len \$m\
    axLimitDescarga]-1)]\r\
    \n        }\r\
    \n        :if (\$valorK) do={ \r\
    \n            :set multiplicador 1000 \r\
    \n            :set maxLimitDescarga [:pick \$maxLimitDescarga 0 ([:len \$m\
    axLimitDescarga]-1)]\r\
    \n        }\r\
    \n        :set maxLimitDescarga (\$maxLimitDescarga * \$multiplicador )\r\
    \n\t\t:if ([:tonum \$maxLimitDescarga] > \$maxDescargaTotal) do={ :set max\
    DescargaTotal [:tonum \$maxLimitDescarga] }\r\
    \n    }\r\
    \n\t:if (\$acumDescarga > \$maxDescargaTotal ) do={\r\
    \n\t\t[/queue simple set target=\$targetTotal max-limit=((\$acumDescarga/1\
    000000) .\"M/\" .  (\$acumDescarga/1000000) . \"M\") numbers=\$colaMadre]\
    \r\
    \n\t} else={\r\
    \n\t\t[/queue simple set target=\$targetTotal max-limit=((\$maxDescargaTot\
    al/1000000) .\"M/\" .  (\$maxDescargaTotal/1000000) . \"M\") numbers=\$col\
    aMadre]\r\
    \n\t}\r\
    \n}\r\
    \n#-----------------------------------------------------------------------\
    ---------------------------------------"
add dont-require-permissions=yes name=crearCola owner=haraujo policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source="#\
    ---------- Variables globales ------------------------------\r\
    \n:global cliente\r\
    \n:global reusoFTTH\r\
    \n:global reusoRF\r\
    \n:global reusoDedicado\r\
    \n#---------- Variables locales ------------------------------\r\
    \n:local maxLimit \"0/0\";\r\
    \n:local limitAt \"0/0\";\r\
    \n:local plan [/ip firewall address-list get \$cliente list]\r\
    \n:local reuso \$reusoDedicado\r\
    \n:local comentarioCola \"\"\r\
    \n:if (\$plan~\"FTTH\") do={  \r\
    \n    :set reuso \$reusoFTTH\r\
    \n    :set comentarioCola \"FTTH\"\r\
    \n} else={\r\
    \n    :if (\$plan~\"Dedicado\") do={\r\
    \n        :nothing\r\
    \n        :set comentarioCola \"Dedicado\"\r\
    \n    } else={\r\
    \n        :set reuso \$reusoRF\r\
    \n        :set comentarioCola \"RF\"\r\
    \n    }\r\
    \n}\r\
    \n:do {\r\
    \n    :local descargaPlan [/queue type get [find name=(\$plan . \"_down\")\
    ] pcq-rate]\r\
    \n    :local cargaPlan [/queue type get [find name=(\$plan . \"_up\")] pcq\
    -rate]\r\
    \n    :set maxLimit (\$cargaPlan . \"/\" . \$descargaPlan )\r\
    \n    :set limitAt ((\$cargaPlan / \$reuso) . \"/\" . (\$descargaPlan / \$\
    reuso))\r\
    \n} on-error={ \r\
    \n    :put (\"No existe un queue type para el plan \" . \$plan)\r\
    \n};\r\
    \n\r\
    \n#Agregamos la cola simple\r\
    \n:do {\r\
    \n    /queue simple add name=[/ip firewall address-list get \$cliente comm\
    ent] max-limit=\$maxLimit limit-at=\$limitAt parent=none\\\r\
    \n        queue=\"default\" target=[/ip firewall address-list get \$client\
    e address] comment=\$comentarioCola;\r\
    \n} on-error={\r\
    \n    :put (\"Problemas agregando la cola de \" . [/ip firewall address-li\
    st get \$cliente comment])\r\
    \n};"
